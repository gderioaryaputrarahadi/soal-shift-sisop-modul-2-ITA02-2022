#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

void runCommand(char command[], char *lines[]);

int main() {
    char *newDirectory[] = {"mkdir", "-p", "/home/outlawx777/shift2/drakor", NULL};
    runCommand("/bin/mkdir", newDirectory);
    char *extractFile[] = {"unzip", "drakor.zip", "-d", "/home/outlawx777/shift2/drakor","*.png", NULL};
    runCommand("/bin/unzip", extractFile);
}

void runCommand(char command[], char *lines[]){
    int status;
    pid_t child_id=fork();
    if(child_id==0)
        execv(command, lines);
    while(wait(&status)>0);
}