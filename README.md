# Soal Shift SisOp Modul 2 ITA02 2022
Anggota Kelompok:

-Asima Prima Yohana Tampubolon 5027201009

-Gde Rio Aryaputra Rahadi 5027201063

-Muhammad Hanif Fatihurrizqi 5027201068

# Soal 1

# 1.a 
Pada soal ini, kita diminta untuk membuat suatu program dimana ketika program tersebut pertama sekali dijalankan akan otomatis mendownload file file characters dan file weapon dari link yang sudah disediakan. Setelah itu, program akan mengekstrak file tersebut. Karena, file sebelumnya masih berupa zip file. Lalu, setelah berhasil mengekstrak maka program akan membuat suatu folder dengan nama "gacha_gacha" yang nantinya akan digunakan sebagai working directory. Seluruh hasil gacha yang dilakukan akan disimpan di dalam folder tersebut.

## Jawab

Pada soal ini, pertama sekali saya melakukan deklarasi variable "char_link" dan "weapon_link" yang akan berfungsi sebagai nama variable dari link dowload file. 

![](img/1.1.png)

Lalu, kita melakukan pengkondisian if dengan bantuan fungsi exists untuk mengecek ketersediaan file. 
    
![](img/1.2.png)

Dimana, apabila pada folder tidak ditemukan file bernama characters dan weapon maka akan mendownload file dari link yang ada lalu disimpan di dalam folder dengan nama "char.zip" dan "weapon.zip". Ketika menjalankan perintah tersebut, saya menggunakan fungsi "runCommand". Pada fungsi tersebut, akan melakukan spawning karena pada saat bersamaan tidak dapat melakukan 2 proses download dalam 1 waktu. Oleh sebab itu, kita menggunakan fork terlebih dahulu.

![](img/1.3.png)

![](img/1.4.png)

Setelah berhasil melakukan download, maka kita melakukan unzip file. Program yang dibuat juga mirip dengan cara mendownload. Hanya fungsi nya saja yang berbeda. Pada langkah ini juga kita menggunakan bantuan fungsi "runCommand" karena file yang akan diekstrak terdapat sebanyak 2 file.

![](img/1.5.png)

## Hasil run

![](img/1.6.png)

## Error yang dialami

Kami mengalami error ketika tidak menggunakan fork. Ketika program dijalankan file yang terdownload hanya 1. Lalu, kami menyadari bahwa tidak dapat mendownload file secara bersamaan pada 1 proses. Oleh sebab itu, kami menggunakan fork sebagai solusi. 

![](img/1.7.png)

## Hasil run ketika error:

![](img/1.8.png)

# Soal 2

# 2a.

Permintaan di dalam subsoal ini dapat diperinci menjadi:

1. Buat direktori baru pada /home/user/ dengan nama directory baru /shift2/drakor

2. Extract isi dari file drakor.zip ke dalam directory /home/user/shift2/drakor dengan memisahkan file penting dan file tidak penting. Dalam hal ini file penting yang dimaksud adalah file dengan format .png saja

## Penyelesaian

Untuk membuat direktori yang diminta maka digunakan perintah

    mkdir -p /home/outlawx777/shift2/drakor

Kemudian untuk mengekstrak isi dari drakor.zip dengan catatan yang diekstrak hanya file .png digunakan perintah

    unzip drakor.zip -d /home/outlawx777/shift2/drakor *.png 

*.png merupakan perintah untuk me-spesifikasikan ekstensi dari file dalam .zip yang akan diekstrak, sisanya tidak akan diekstrak ke direktori

Rincian kode dalam bahasa C

    char *newDirectory[] = {"mkdir", "-p", "/home/outlawx777/shift2/drakor", NULL};
    runCommand("/bin/mkdir", newDirectory);
    char *extractFile[] = {"unzip", "drakor.zip", "-d", "/home/outlawx777/shift2/drakor","*.png", NULL};
    runCommand("/bin/unzip", extractFile);

Untuk fungsi runCommand() sendiri berfungsi agar kedua perintah tersebut tidak dijalankan secara bersamaan, melainkan berurutan

Rincian fungsi runCommand()

    void runCommand(char command[], char *lines[]){
    int status;
    pid_t child_id=fork();
    if(child_id==0)
        execv(command, lines);
    while(wait(&status)>0);
    }

Di dalam fungsi ini digunakan fork untuk spawn child process baru, kemudian terdapat fungsi wait yang akan menunggu child process selesai melakukan tugasnya, dalam konteks ini membuat direktori, barulah process unzip dapat berjalan.

## Hasil dari menjalankan program

![](img/2.1.png) 

Gambar 2.1 hasil kompilasi dengan gcc dan run di terminal

![](img/2.2.png) 

Gambar 2.2 hasil setelah program di-run. Terbentuk direktori /shift2/drakor dan di dalam folder drakor berisi file-file .png dari drakor.zip

# Soal 3

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang

# Soal 3a

Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

## Penyelesaian

![](img/3.1.JPG)

1. Parent process akan membuat child process dimana dia akan membuat direktori pada /home/hanif/     modul2/darat menggunakan perintah mkdir. Disini argument -p digunakan agar directory yang belum dibuat (seperti /home/hanif/modul2) langsung terbuat.

2. Lalu parent process akan menunggu mkdir selesai dibuat, lalu akan sleep() selama 3 detik. Lalu parent process akan membuat child process lagi untuk membuat direktori pada /home/hanif/modul2/air. Lalu parent process akan menunggu child process selesai.

# Soal 3b

Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.

## Penyelesaian

Untuk meng-ekstrak file animal.zip dapat menggunakan perintah unzip.

![](img/3.2.JPG)

* argument -q digunakan agar process unzip tidak mengeluarkan output ke terminal.
* argument -d untuk menyatakan directory output hasil ekstrak.

## Hasil Run

![](img/3.3.JPG)

![](img/3.4.JPG)
