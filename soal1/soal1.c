#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

int exists(const char *fname)
{
    DIR* dir = opendir(fname);
    if (dir){
        closedir(dir);
        return 1;
    } else if(ENOENT == errno){
        return 0;
    }
}

void runCommand(char command[], char *lines[]){
    int status;
    pid_t child_id=fork();
    if(child_id==0)
        execv(command, lines);
    while(wait(&status)>0);
}

int main(){
	char *char_link = "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp";
	char *weapon_link = "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT";

    // Download database
    if((exists("characters") == 0) && (exists("weapons") == 0)){
		char *getChar[] = {"wget", char_link, "-O", "char.zip", "-o", "/dev/null", NULL};
			runCommand("/usr/bin/wget", getChar);
        char *getWeapon[] = {"wget", weapon_link, "-O", "weapon.zip", "-o", "/dev/null", NULL};
			runCommand("/usr/bin/wget", getWeapon);
    }
   	// Extact File
   	char *extractFileChar[] = {"unzip", "char.zip", "-d", "char", NULL};
    runCommand("/bin/unzip", extractFileChar);
	char *extractFileWeapon[] = {"unzip", "weapon.zip", "-d", "weapon", NULL};
    runCommand("/bin/unzip", extractFileWeapon);

}
