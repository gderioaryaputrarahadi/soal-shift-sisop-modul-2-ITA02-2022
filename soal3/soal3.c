#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <wait.h>

int main() {

  pid_t child_id = fork();
  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", "/home/hanif/modul2/darat", NULL};
    execv("/bin/mkdir", argv);
  }
  while(wait(NULL) != child_id);
  sleep(3);

  child_id = fork();
  if (child_id == 0) {
    char *argv[] = {"mkdir", "-p", "/home/hanif/modul2/air", NULL};
    execv("/bin/mkdir", argv);
  }
  while(wait(NULL) != child_id);

  child_id = fork();
  if (child_id == 0) {
    char *argv[] = {"unzip", "-q", "animal.zip", "-d", "/home/hanif/modul2", NULL};
    execv("/usr/bin/unzip", argv);
  }
  while(wait(NULL) != child_id);
}